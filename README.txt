CSCI 40 - E

Arpas, Matthew Karl David, P. - 210493
Cruz, Calvin Josh, G. - 211677
Gagalac, Albert Emmanuel, B. - 192102
Lopez, Michael, T. II – 213546
Oarde, Carlos Gabriel, R. - 214279

Final Project: Widget v2

App Assignments:

Dashboard - Carlos
Announcement - Michael
Forum - Calvin
Assignments - Albert
Calendar - Matt

Date of Submission: May 15, 2023

The project was truthfully completed by the five of us in the appropriate specifications and with the proper workflow in Git and Gitlab. We tried our best to make our code as coherent as possible - and to the best of our knowledge we were able to do so by making sure we were referring to the same code for snippets, namely our latest Lab.

References:

CSCI 40 Labs

(sgd) Matthew Karl David P. Arpas - 5/10/2023
(sgd) Cruz, Calvin Josh, G. - 5/10/2023
(sgd) Gagalac, Albert Emmanuel, B. - 5/10/2023
(sgd) Lopez, Michael, T. II - 5/10/2023
(sgd) Oarde, Carlos Gabriel, R. - 5/10/2023