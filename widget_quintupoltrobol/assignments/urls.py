from django.urls import path

from .views import pageview, AssignmentDetailView, AssignmentCreateView, AssignmentEditView

urlpatterns = [
    path('', pageview, name='pageview'),
    path('<int:pk>/details', AssignmentDetailView.as_view(), name='assignment-details'),
    path('add', AssignmentCreateView.as_view(), name='assignment-add'),
    path('<int:pk>/edit', AssignmentEditView.as_view(), name='assignment-edit'),
]

app_name = "assignments"