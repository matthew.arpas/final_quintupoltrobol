from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator

# Create your models here.

class Course(models.Model):
    code = models.CharField(default="", max_length=10)
    title = models.CharField(default="", max_length=50)
    section = models.CharField(
        default="",max_length=3, 
        unique=True, 
        validators=[RegexValidator(r'^[a-zA-Z]*$',
        message='Only letters are allowed')])
    
    def __str__(self):
        return "%s %s - %s" % (self.code, self.title, self.section)
    
    
       
class Assignment(models.Model):
    
    name = models.CharField(default="", max_length=50)
    description = models.TextField(default="", max_length=700)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    perfect_score = models.PositiveIntegerField(default=0)
    
    def get_absolute_url(self):
        return reverse('assignments:assignment-details', kwargs={'pk' : self.pk})
    
    def __str__(self):
        return self.name
    
    def get_passing_score(self):
        pass_score = self.perfect_score * (60/100)
        return pass_score
    
    @property
    def passing_score(self):
        return self.get_passing_score()
    
    
    