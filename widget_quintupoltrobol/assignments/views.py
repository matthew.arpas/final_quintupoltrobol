from django.shortcuts import render
from .models import Assignment, Course
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

def pageview(request):
    all_assignment = Assignment.objects.all()
    all_course = Course.objects.all()
    
    return render(request, 'assignments/assignments.html', {'assignment': all_assignment, 'course': all_course})

class AssignmentDetailView(DetailView):
    def get(self, request, pk):
        assignment = Assignment.objects.get(pk=pk)
        course = assignment.course
        
        return render(request, 'assignments/assignment-details.html', {'assignment': assignment, 'course': course})

class AssignmentCreateView(CreateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-add.html'

class AssignmentEditView(UpdateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-edit.html'
    