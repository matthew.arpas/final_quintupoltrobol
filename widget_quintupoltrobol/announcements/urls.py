from django.urls import path
from .views import pageview, AnnouncementDetailView, AnnouncementCreateView, AnnouncementEditView

urlpatterns = [
    path('', pageview, name='pageview'),
    path('<int:pk>/details/', AnnouncementDetailView.as_view(), name="announcement_details"),
    path('add/', AnnouncementCreateView.as_view(), name='announcement_add'),
    path('<int:pk>/edit/', AnnouncementEditView.as_view(), name='announcement_edit'),
]

app_name = "announcements"