from .forms import AnnouncementForm
from .models import Announcement, Reaction
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.db.models import Sum
from django.shortcuts import render
from django.urls import reverse
from .models import Announcement

def pageview(request):
    model_object = Announcement.objects.order_by("-pub_datetime")
    return render(request, "announcements/announcements.html", context={"announcement_list":model_object})


class AnnouncementListView(ListView):
    model = Announcement
    template_name = "announcements/announcements.html"

class AnnouncementDetailView(DetailView):
    model = Announcement
    template_name = "announcements/announcement-detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reactions = Reaction.objects.filter(announcement=self.object)

        context['like_count'] = reactions.filter(name='Like').aggregate(total=Sum('tally'))['total'] or 0
        context['love_count'] = reactions.filter(name='Love').aggregate(total=Sum('tally'))['total'] or 0
        context['angry_count'] = reactions.filter(name='Angry').aggregate(total=Sum('tally'))['total'] or 0
        return context

class AnnouncementCreateView(CreateView):
    model = Announcement
    form_class = AnnouncementForm
    template_name = "announcements/announcement-add.html"

    def get_success_url(self):
        return reverse("announcements:announcement_details", kwargs={"pk": self.object.pk})
    
class AnnouncementEditView(UpdateView):
    model = Announcement
    form_class = AnnouncementForm
    template_name = "announcements/announcement-edit.html"

    def get_success_url(self):
        return reverse("announcements:announcement_details", kwargs={"pk": self.object.pk})




