from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse
from django.utils import timezone

class Announcement(models.Model):
  title = models.CharField(max_length=50)
  body = models.TextField(max_length=1024)
  author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
  pub_datetime = models.DateTimeField(default=timezone.now)

  def __str__(self):
    return self.title
  
  def get_absolute_url(self):
    return reverse("announcements:announcement_details", kwargs={"pk": self.pk})


class Reaction(models.Model):
  name_choices = [
        ("Like", 'Like'),
        ("Love", 'Love'), 
        ("Angry", 'Angry')
    ]
  name = models.CharField(
        max_length=10,
        choices=name_choices,
    )

  tally = models.IntegerField()
  announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE)

  def __str__(self):
    return self.name

