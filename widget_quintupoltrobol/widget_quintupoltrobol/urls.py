from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('', include('dashboard.urls', namespace="dashboard")),
    path('announcements/', include('announcements.urls', namespace="announcements")),
    path('forum/', include('forum.urls', namespace="forum")),
    path('assignments/', include('assignments.urls', namespace="assignments")),
    path('widget_calendar/', include('widget_calendar.urls', namespace="widget_calendar")),
    path('admin/', admin.site.urls),
]
