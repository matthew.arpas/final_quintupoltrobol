from django.shortcuts import render
from .models import ForumPost, Reply
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

def pageview(request):
    forumposts = ForumPost.objects.order_by("-pub_datetime")
    replys = Reply.objects.all()
    
    return render(request, 'forum/forum.html', {'forumposts': forumposts, 'replys': replys})
    
class ForumpostDetailView(DetailView):
    def get(self, request, pk):
        forumpost = ForumPost.objects.get(pk=pk)
        replys = Reply.objects.filter(forum_post=forumpost)
        return render(request, 'forum/forumpost-details.html', {'forumpost': forumpost, 'replys': replys})
        
class ForumpostCreateView(CreateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-add.html'
    
class ForumpostUpdateView(UpdateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-edit.html'
