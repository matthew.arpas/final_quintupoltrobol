from django.urls import path
from .views import pageview, ForumpostDetailView, ForumpostCreateView, ForumpostUpdateView

urlpatterns = [
    path('', pageview, name='pageview'),
    path('forumposts/<int:pk>/details/', ForumpostDetailView.as_view(), name='forumpost-details'),
    path('forumposts/add/', ForumpostCreateView.as_view(), name='forumpost-add'),
    path('forumposts/<int:pk>/edit/', ForumpostUpdateView.as_view(), name='forumpost-edit'),
]

app_name = "forum"