from django import forms
from .models import ForumPost

class ForumpostForm(forms.ModelForm):
	class Meta:
		model = ForumPost
		fields = ['title', 'body', 'author']