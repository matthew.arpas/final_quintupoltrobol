from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse

class ForumPost(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField()
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse("forum:forumpost-details", kwargs={"pk": self.pk})
    
class Reply(models.Model):
    body = models.TextField()
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField()
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.body
