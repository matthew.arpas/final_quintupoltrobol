from django.contrib import admin

from .models import ForumPost, Reply

class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost
    list_display = ["title", "body", "author"]

class ReplyAdmin(admin.ModelAdmin):
    model = Reply
    list_display = ["body", "author", "forum_post"]
    
admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)