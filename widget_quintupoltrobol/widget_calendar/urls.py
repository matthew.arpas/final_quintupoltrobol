from django.urls import path

from .views import pageview, Widget_CalendarDetailView, Widget_CalendarCreateView, Widget_CalendarEventUpdateView

urlpatterns = [
    path('', pageview, name='pageview'),
    path('widget_calender/events/<int:pk>/details', Widget_CalendarDetailView.as_view(), name='event-details'),
    path('widget_calender/add', Widget_CalendarCreateView.as_view(), name='event-add'),
    path('widget_calender/events/<int:pk>/edit', Widget_CalendarEventUpdateView.as_view(), name='event-edit'),
]

app_name = "widget_calendar"