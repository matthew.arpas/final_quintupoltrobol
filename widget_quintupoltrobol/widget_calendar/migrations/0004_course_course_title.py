# Generated by Django 4.1.6 on 2023-05-15 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('widget_calendar', '0003_course_alter_event_course'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_title',
            field=models.CharField(default='a', max_length=50),
            preserve_default=False,
        ),
    ]
