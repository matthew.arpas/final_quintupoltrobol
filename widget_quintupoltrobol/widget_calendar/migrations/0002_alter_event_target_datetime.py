# Generated by Django 4.1.6 on 2023-03-04 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('widget_calendar', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='target_datetime',
            field=models.CharField(max_length=50),
        ),
    ]
