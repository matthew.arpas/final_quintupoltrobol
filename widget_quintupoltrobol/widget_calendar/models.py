from django.db import models
from django.urls import reverse
    
class Location(models.Model):
    modechoices = [
        ('onsite', 'onsite'),
        ('online', 'online'), 
        ('hybrid', 'hybrid')
    ]
    mode = models.CharField(
        max_length=6,
        choices=modechoices,
        default='onsite'
    )
    venue = models.TextField()

    def __str__(self):
        return '{}, {}'.format(self.mode, self.venue)
    
class Course(models.Model):
    course_code = models.CharField(max_length=10)
    course_title = models.CharField(max_length=50)
    section = models.CharField(max_length=5)

    def __str__(self):
        return self.course_title
    
class Event(models.Model):
    target_datetime = models.CharField(max_length=50)
    activity = models.CharField(max_length=300)
    estimated_hours = models.FloatField()
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.activity
    
    def get_absolute_url(self):
        return reverse('widget_calendar:event-details', kwargs={"pk": self.pk})