from django.contrib import admin
from .models import Event, Course, Location

class EventAdmin(admin.ModelAdmin):
    model = Event

class CourseAdmin(admin.ModelAdmin):
    model = Course

class LocationAdmin(admin.ModelAdmin):
    model = Location

admin.site.register(Event, EventAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Location, LocationAdmin)