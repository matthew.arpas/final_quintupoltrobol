from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import Event, Location

def pageview(request):
    events = Event.objects.all()
    locations = Location.objects.all()

    return render(request, 'widget_calendar/widget_calendar.html', {'events': events, 'locations': locations})

class Widget_CalendarDetailView(DetailView):
    def get(self, request, pk):
        event = Event.objects.get(pk=pk)
        course = event.course
        location = event.location

        return render(request, 'widget_calendar/event-details.html', {'event': event, 'course': course, 'location': location})

class Widget_CalendarCreateView(CreateView):
    model = Event
    fields = '__all__'
    template_name = 'widget_calendar/event-add.html'

class Widget_CalendarEventUpdateView(UpdateView):
    model = Event
    fields = '__all__'
    template_name = 'widget_calendar/event-edit.html'