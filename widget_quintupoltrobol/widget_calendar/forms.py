from django import forms
from .models import Event

class WidgetUserForm(forms.ModelForm):
    class Meta:
            model = Event
            fields = ['activity', 'target_datetime', 'estimated_hours', 'location', 'course',]