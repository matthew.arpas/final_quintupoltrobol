from django.contrib import admin
from .models import Department, WidgetUser

class DepartmentAdmin(admin.ModelAdmin):
    model = Department

class WidgetUserAdmin(admin.ModelAdmin):
    model = WidgetUser

admin.site.register(Department, DepartmentAdmin)
admin.site.register(WidgetUser, WidgetUserAdmin)