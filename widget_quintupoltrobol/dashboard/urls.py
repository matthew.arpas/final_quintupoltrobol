from django.urls import path
from .views import WidgetUserDetailView, WidgetUserCreateView, WidgetUserUpdateView

from .views import pageview

urlpatterns = [
    path('dashboard/', pageview, name='pageview'),
    path('widgetusers/<int:pk>/details', WidgetUserDetailView.as_view(), name='widgetuser-details'),
    path('widgetusers/add', WidgetUserCreateView.as_view(), name='widgetuser-add'),
    path('widgetusers/<int:pk>/edit', WidgetUserUpdateView.as_view(), name='widgetuser-edit'),
]

app_name = "dashboard"