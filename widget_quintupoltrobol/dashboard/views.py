from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import Department, WidgetUser

def pageview(request):
    departments = Department.objects.all()
    widgetusers = WidgetUser.objects.all()

    return render(request, 'dashboard/dashboard.html', {'departments': departments, 'widgetusers': widgetusers})

class WidgetUserDetailView(DetailView):
    def get(self, request, pk):
        user = WidgetUser.objects.get(pk=pk)
        department = user.department
        return render(request, 'dashboard/widgetuser-details.html', {'department': department, 'user': user})
    
class WidgetUserCreateView(CreateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-add.html'

class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    fields = '__all__'
    template_name = 'dashboard/widgetuser-edit.html'
    